package application;

import java.text.DecimalFormat;
import java.util.EventListener;

import com.sun.glass.ui.TouchInputSupport;

import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;

// class definition
public class Projectile extends Application {

	//Layout
	private GridPane gp = new GridPane();

	//Projectile Type
	private Label projectile_type_label = new Label("Projectile Type");
	private Label projectile_image_label = new Label();
	private ComboBox<String> projectile_type_combobox = new ComboBox<String>();
	
	// Mass
	private Label mass_label = new Label("Mass (kgs)");
	private TextField mass_textField = new TextField();
	private double mass;

	//Angle
	private Label angle_label = new Label("Angle (degrees)");
	private Slider angle_slider = new Slider(0, 40, 20);
	private TextField angle_textField = new TextField();
	private double angle;
	
	//Formating the values in the duration box
	private DecimalFormat df = new DecimalFormat();

	//Initial Speed 
	private Label initial_speed_label = new Label("Initial Speed (m/s)");
	private ToggleGroup initial_speed_toggleGroup = new ToggleGroup();
	private RadioButton initial_speed_slow = new RadioButton("slow");
	private RadioButton initial_speed_medium = new RadioButton("medium");
	private RadioButton initial_speed_fast = new RadioButton("fast");
	private TextField intitial_speed_textField = new TextField();
	private double initial_speed;
	
	private HBox intitial_speed_container = new HBox(initial_speed_slow ,initial_speed_medium, initial_speed_fast);

	//Range
	private Label range_label = new Label("Range (m)");
	private TextField range_textField = new TextField();
	private double range;

	//Height
	private Label height_label = new Label("Max Height (m)");
	private TextField height_textField = new TextField();
	private double height;

	//Time
	private Label time_label = new Label("Time (s)");
	private TextField time_textField = new TextField();
	private double time; 

	//Gravity 
	private static final double gravitational_accelleration = 9.81; // m/s/s

	//Calculate
	private Button fire_button = new Button("Fire");
	private Button erase_button = new Button("Erase");
	
	//Bonus
	private Image canon_image = new Image(getClass().getResourceAsStream("fire.jpg"));
	private Image human_image = new Image(getClass().getResourceAsStream("human.jpg"));
	private Image piano_image = new Image(getClass().getResourceAsStream("piano.jpg"));
	private Image ball_image = new Image(getClass().getResourceAsStream("ball.gif"));
	AnimationTimer fire_timer = new AnimationTimer() {
		
		@Override
		public void handle(long now) {
			// TODO Auto-generated method stub
			projectile_image_animation_label.setTranslateX(range);
		}
	};
	
	private Label canon_image_label = new Label();
	private Label projectile_image_animation_label = new Label();
	
	// init method
	public void init() {

		//Init Buttons
		this.fire_button.setStyle("-fx-text-fill: #FFFFFF; -fx-background-color: #ff0000; ");
		this.erase_button.setStyle("-fx-text-fill: #FFFFFF; -fx-background-color: #0000FF; ");		
		
		//Init ToggleGroup
		this.initial_speed_slow.setToggleGroup(this.initial_speed_toggleGroup);
		this.initial_speed_medium.setToggleGroup(this.initial_speed_toggleGroup);
		this.initial_speed_fast.setToggleGroup(this.initial_speed_toggleGroup);
		
		//Init ComboBox
		this.projectile_type_combobox.getItems().addAll("Piano", "Adult Human");
		this.projectile_type_combobox.setValue("Piano");
		
		//Bonus
		this.canon_image_label.setGraphic(new ImageView(this.canon_image));
		
		// Inital Speed ToggleGroup
		//use the .setUserData command of the radio button to store speeds
		initial_speed_slow.setUserData("10");
		initial_speed_medium.setUserData("50");
		initial_speed_fast.setUserData("100");
		
		// Prevent the following TextFields from being editable: angle,intial speed range, height, time 
		
		this.mass_textField.setTextFormatter(new TextFormatter<>(new NumberStringConverter()));
		this.angle_textField.setEditable(false);
		this.intitial_speed_textField.setEditable(false);
		this.range_textField.setEditable(false);
		this.height_textField.setEditable(false);
		this.time_textField.setEditable(false);
		
		
		// Layout controls as per the diagram, feel free to improve the UI. 
		// How many rows and columns do you want - work this out on paper first 
		// My version has 7 rows, you can look at the JavaFX API to see how to get controls to span more than one column
		gp.addRow(0, this.projectile_type_label, this.projectile_type_combobox, this.projectile_image_label);
		gp.addRow(1, this.mass_label, this.mass_textField);
		gp.addRow(2, this.angle_label, this.angle_textField, this.angle_slider);
		gp.addRow(3, this.initial_speed_label, this.intitial_speed_textField, this.intitial_speed_container);
		gp.addRow(4, this.range_label, this.range_textField);
		gp.addRow(5, this.height_label, this.height_textField);
		gp.addRow(6, this.time_label, this.time_textField);
		gp.addRow(7, this.fire_button, this.erase_button);	
		gp.addRow(8, this.canon_image_label, this.projectile_image_animation_label);	
		
		//Debug
		//gp.setGridLinesVisible(true);
		
		// Method call (not declaration!)  to initialize the controls based on the projectile type.
		this.initalizeControlValues();
		
		//  Listener for angle Slider to set angle TextTield and the angle variable 
		angle_slider.valueProperty().addListener(new ChangeListener<Number>(){
			public void changed(final ObservableValue<? extends Number> observable, final Number oldValue, final Number newValue){
				angle = (double) newValue;
				angle_textField.setText("" + newValue);
			}
		});

		// Listener for inital_speed ToggleGroup to set initital_speed TextField
		this.initial_speed_toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			
			@Override
			public void changed(ObservableValue<? extends Toggle> ov, Toggle toggle,Toggle new_toggle) {
				if (initial_speed_toggleGroup.getSelectedToggle() != null) {
				
					initial_speed = Double.parseDouble((String) initial_speed_toggleGroup.getSelectedToggle().getUserData());
					intitial_speed_textField.setText("" + initial_speed);
				}
			}
		});

		// Listener to call the fire() method when the fire button is pressed
		this.fire_button.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				fire();
				
			}
		});
		
		// Listener to initialize control values if the projectile type is changed
		this.projectile_type_combobox.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				initalizeControlValues();
			}
		});
		

		// Listener to initialize control values if the erase button is pressed
		this.erase_button.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				range_textField.clear();
				height_textField.clear();
				time_textField.clear();
			}
		});
		
	}

	// Overridden start method
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Projectile Luc-Olivier Merson");
		primaryStage.setScene(new Scene(this.gp, 400, 350));
		primaryStage.show();
	}

	// Overridden stop method add functionality to this if you wish.
	public void stop() {

	}

	// Method to harvest values from controls, perform calculation and display the results
	private void fire(){
		//capture the values from the text fields outputting number errors where relevant 
		if (this.mass_textField.getText().isEmpty()) {
			Alert fail= new Alert(AlertType.INFORMATION);
			fail.setHeaderText("failure");
	        fail.setContentText("you havent typed something");
	        fail.showAndWait();
	        this.initalizeControlValues();
	        return ;
		} /*else if (this.mass_textField.getText()) {
			Alert fail= new Alert(AlertType.INFORMATION);
			fail.setHeaderText("failure");
	        fail.setContentText("you havent typed something");
	        fail.showAndWait();
	        this.initalizeControlValues();
	        return ;
		}*/
		this.mass = Double.parseDouble(this.mass_textField.getText());
		this.angle = this.angle_slider.getValue();
		this.initial_speed = Double.parseDouble((String) initial_speed_toggleGroup.getSelectedToggle().getUserData());
		
		// don't forget to convert your angle input to radians for use with Math.sin()
		this.angle = Math.PI * this.angle / 180;
		
		System.out.println("mass :" + this.mass);
		System.out.println("angle :" + this.angle);
		System.out.println("initial speed :" + this.initial_speed);
		
		// calculate the range of the projectile	
		this.range = ((Math.pow(this.initial_speed, 2) / this.gravitational_accelleration) * Math.sin(2 * this.angle));	
		
		// calculate the flight time of the projectile
		this.time = (2 * this.initial_speed * Math.sin(this.angle)) / this.gravitational_accelleration;
		
		// calculate the max height of the projectile
		this.height = (Math.pow(this.initial_speed, 2) * Math.pow(Math.sin(this.angle), 2)) / (2 * gravitational_accelleration);
		
		// display the results in the relevant TextFields
		this.height_textField.setText("" + this.height);
		this.range_textField.setText("" + this.range);
		this.time_textField.setText("" + this.time);
		
		//Animation
		//this.projectile_image_animation_label.setGraphic(this.projectile_image_label.getGraphic());
		this.fire_timer.start();
	}

	// Method to initalize the controls based on the selection of the projectile type 
	private void initalizeControlValues() {
		if (this.projectile_type_combobox.getValue() == "Adult Human") {
			//Refresh mass value and TextField for mass
			this.mass = 80;
			this.mass_textField.setText("" + this.mass);
			
			//Init Slider
			this.angle_slider.setMin(0);
			this.angle_slider.setMax(90);
			this.angle_slider.setValue(45);
			this.angle_slider.setMajorTickUnit(15);
		
			//Bonus
			this.projectile_image_label.setGraphic(new ImageView(this.human_image));
			
			this.initial_speed_fast.setSelected(true);
			this.intitial_speed_textField.setText((String) this.initial_speed_fast.getUserData());
		} else {
			//Refresh mass value and TextField for mass
			this.mass = 400;
			this.mass_textField.setText("" + this.mass);		
			
			//Init Slider
			this.angle_slider.setMin(0);
			this.angle_slider.setMax(40);
			this.angle_slider.setValue(20);
			this.angle_slider.setMajorTickUnit(10);
			
			//Bonus
			this.projectile_image_label.setGraphic(new ImageView(this.piano_image));
			
			this.initial_speed_slow.setSelected(true);
			this.intitial_speed_textField.setText((String) this.initial_speed_slow.getUserData());
		}
		
		//Get angle
		this.angle = this.angle_slider.getValue();
		this.angle_textField.setText("" + this.angle_slider.getValue());
		
		//Get initial speed
		this.initial_speed = Double.parseDouble(this.intitial_speed_textField.getText());
		
		// display ticks etc
		this.angle_slider.setShowTickMarks(true);
		this.angle_slider.setShowTickLabels(true);
		this.angle_slider.setBlockIncrement(1);
		
		// clear the results fields and variables		
		range_textField.clear();
		height_textField.clear();
		time_textField.clear();

	}

	// Entry point to our program
	public static void main(String[] args) {
		launch(args);
	}
}

